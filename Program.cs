﻿using System.Diagnostics;
using System.Reflection.Metadata;
using System.Data;
using System;
using System.Collections.Generic;

namespace tictoctoe {
    class Program {

        public const int nbLines = 5;
        public const int nbCols = 5;
        public static char[,] gameBoard = new char[nbLines,nbCols] { // lines, cols
            {' ', '|', ' ', '|', ' '},
            {'-', '+', '-', '+', '-'},
            {' ', '|', ' ', '|', ' '},
            {'-', '+', '-', '+', '-'},
            {' ', '|', ' ', '|', ' '}
        };
        public static List<string> alreadyUsed = new List<string>();

        static bool checkIfUsed(string pos) {
            if (!alreadyUsed.Contains(pos)) {
                alreadyUsed.Add(pos);
                return false;
            }
            return true;
        }
        static void printGameBoard(char[,] gameBoard) {
            for (int i=0; i<nbLines; i++) {
                for (int j=0; j<nbCols; j++)
                    Console.Write(gameBoard[i, j]);
                Console.WriteLine();
            }
        }

        unsafe static bool checkPiece(char* piece, char userPos) {
            if (*piece == ' ') {
                *piece = userPos;
                return true;
            } else
                return false;
            /*
            Console.WriteLine("piece = {0}", piece);
            return false;
            */
        }
        static bool placePiece(char[,] gameBoard, int pos, string user) {
            
            char symbol = (user == "player") ? 'X' : 'O';
            unsafe {switch (pos) {
                case 1:
                    fixed( char* p = &gameBoard[0,0]) return checkPiece(p, symbol);
                case 2:
                    fixed( char* p = &gameBoard[0,2]) return checkPiece(p, symbol);
                case 3:
                    fixed( char* p = &gameBoard[0,4]) return checkPiece(p, symbol);
                case 4:
                    fixed( char* p = &gameBoard[2,0]) return checkPiece(p, symbol);
                case 5:
                    fixed( char* p = &gameBoard[2,2]) return checkPiece(p, symbol);
                case 6:
                    fixed( char* p = &gameBoard[2,4]) return checkPiece(p, symbol);
                case 7:
                    fixed( char* p = &gameBoard[4,0]) return checkPiece(p, symbol);
                case 8:
                    fixed( char* p = &gameBoard[4,2]) return checkPiece(p, symbol);
                case 9:
                    fixed( char* p = &gameBoard[4,4]) return checkPiece(p, symbol);
                default:
                    if (pos > 9)
                        Console.WriteLine("INVALID POSITION !");
                    else
                        Console.WriteLine("Already taken! Choose another Placement : ");
                    return false;
            }}
        }

        static char checkIfSomeoneWon(char[,] gameBoard) {
            int lineCounter=0, colCounter=2;
            char previousVal=gameBoard[0,0];
            string direction = "lines";
            int iterationCount = 24; // security limit

            try {
                while (iterationCount>0) {
                    iterationCount--;
                    if (direction == "lines") {
                        if (previousVal != ' ' && gameBoard[lineCounter, colCounter] == previousVal) {
                            if(colCounter==4)
                                return previousVal;
                            else
                                colCounter+=2;
                        } else {
                            if (lineCounter<4) {
                                lineCounter+=2;
                                colCounter = 2;
                                previousVal = gameBoard[lineCounter, 0];
                            } else {
                                direction = "cols";
                                colCounter = 0;
                                lineCounter = 2;
                                previousVal = gameBoard[0, 0];
                            }
                        }
                    } else if (direction == "cols") {
                        if (previousVal != ' ' && gameBoard[lineCounter, colCounter] == previousVal) {
                            if(lineCounter==4)
                                return previousVal;
                            else
                                lineCounter+=2;
                        } else {
                            if (colCounter<4) {
                                colCounter+=2;
                                previousVal = gameBoard[0, colCounter];
                                lineCounter = 2;
                            } else {
                                direction = "diag1";
                                colCounter = 2;
                                lineCounter = 2;
                                previousVal = gameBoard[0,0];
                            }
                        }
                    } else if (direction == "diag1") {
                        if (previousVal != ' ' && gameBoard[lineCounter, colCounter] == previousVal) {
                            if(lineCounter==4)
                                return previousVal;
                            else {
                                lineCounter+=2;
                                colCounter+=2;
                            }
                        } else {
                            if(lineCounter==4) {
                                direction = "diag2";
                                colCounter = 2;
                                lineCounter = 2;
                                previousVal = gameBoard[0,4];
                            }
                        }
                    } else {
                        if (previousVal != ' ' && gameBoard[lineCounter, colCounter] == previousVal) {
                            if(lineCounter==0)
                                return previousVal;
                            else {
                                lineCounter-=2;
                                colCounter+=2;
                            }
                        } else {
                            if(previousVal != ' ' && lineCounter>0) {
                                lineCounter-=2;
                                colCounter+=2;
                            }
                        }
                    }
                }
            } catch (System.Exception) {
                
                throw;
            }
            return 'N';
        }

        static void Main(string[] args) {

            char someoneWon = 'N';
            int nbAttemps = 0;
            while (nbAttemps < 9 && someoneWon == 'N') {
                printGameBoard(gameBoard);
                Console.WriteLine("Enter Your Placement : ");
                string playerPos = Console.ReadLine();
                while (checkIfUsed(playerPos)) {
                    Console.WriteLine("Already taken! Choose another Placement : ");
                    playerPos = Console.ReadLine();
                }
                placePiece(gameBoard, int.Parse(playerPos), "player");
                someoneWon = checkIfSomeoneWon(gameBoard);
                if (someoneWon == 'X' || alreadyUsed.Count > 8)
                    break;

                Random rand = new Random();
                int cpuPos = rand.Next(9) +1;
                while (checkIfUsed(cpuPos.ToString())) {
                    Console.WriteLine("Already taken.");
                    cpuPos = rand.Next(9) +1;
                }
                placePiece(gameBoard, cpuPos, "cpu");
                someoneWon = checkIfSomeoneWon(gameBoard);
                nbAttemps++;
            }
            printGameBoard(gameBoard);
            Console.WriteLine("{0} Won", someoneWon);
        }
    }
}
